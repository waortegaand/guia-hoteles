
    $(function(){
        $("[data-bs-toggle='tooltip']").tooltip();
        $("[data-bs-toggle='popover']").popover();
        $('#myCarousel').carousel({interval:5000});
        $('#staticBackdrop').on('show.bs.modal',function(e){
          console.log('El modal se esta visualizando.');
          $('#contactoBtn').removeClass('btn-primary');
          $('#contactoBtn').addClass('btn-secondary');
          $('#contactoBtn').prop('disabled',true);
          }) /**/
        $('#staticBackdrop').on('shown.bs.modal',function(e){
          console.log('El modal se ha visualizado.');
          }) /**/
        $('#staticBackdrop').on('hide.bs.modal',function(e){
          console.log('El modal se oculta.');
          }) /**/
        $('#staticBackdrop').on('hidden.bs.modal',function(e){
          console.log('El modal se ha ocultado.');
          $('#contactoBtn').prop('disabled',false);
          }) /**/
      });
      /*
      var myCarousel = document.querySelector('#myCarousel')
      var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 10000,
        wrap: false
      })
      */